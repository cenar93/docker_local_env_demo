import React, { useState, useEffect } from 'react';
import './App.css';

interface AppProps { }

function App({ }: AppProps) {
  // Create the count state.
  const [backendResponse, setBackendResponse] = useState<string>("");
  // Create the counter (+1 every second).
  useEffect(() => {
    fetch(`http://localhost:8080/hello`)
      .then((res) => res.text())
      .then(setBackendResponse)
  }, []);
  // Return the App component.
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Response from backend: <code>{backendResponse}</code>.
        </p>
      </header>
    </div>
  );
}

export default App;
