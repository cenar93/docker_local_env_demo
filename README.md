
#### Architecture
![Architecture](./docs/assets/arcitecture.svg)

### Running locally
#### Commands
##### Build containers
This is only required if you change something on Dockerfiles
````
docker-compose build
````
##### Start both backend and frontend
````
docker-compose up
````
##### Stop and delete both backend and frontend container
````
docker-compose down
````
#### docker-compose run
docker-compose run command does not create any of the ports specified in the service configuration. This prevents port collisions with already-open ports. If you do want the service’s ports to be created and mapped to the host, specify the --service-ports flag (https://docs.docker.com/compose/reference/run)
##### Start only single service
````
docker-compose run --service-ports frontend
````

##### Open shell into single container
````
docker-compose run --service-ports frontend sh
````

##### Run any command in container
This ignores the command defined in compose file and executes the specified command instead
````
docker-compose run --service-ports frontend <any command>
````